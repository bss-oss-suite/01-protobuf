# 02-protobuf
Part of project BSS/OSS suite.


- Generate protobuf for back-end:

```shell
protoc --go_out=. --go_opt=paths=source_relative \
       --go-grpc_out=. --go-grpc_opt=paths=source_relative \
       authentication.proto

```

- Then we can move generated files to microservices location.

- Generate protobuf for front-end:

```shell
protoc --js_out=import_style=commonjs:. --grpc-web_out=import_style=commonjs,mode=grpcwebtext:. authentication.proto

```

- Then we can move generated files to microservices location.

